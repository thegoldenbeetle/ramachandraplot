import matplotlib.pyplot as plt
#import matplotlib.patches as mpatches
#import matplotlib.colors as mplcolors

from aminoacid import AMINO_ACIDS
import plot_parameters
import get_data_from_file


def get_ordinary_plot(df, aminoacids, title):
    fig, ax = plt.subplots()
    
    aminoacids_objects = filter(lambda aminoacid: aminoacid.name in aminoacids, AMINO_ACIDS)
    for aminoacid in aminoacids_objects:
        ax.scatter(list(df[df[get_data_from_file.AMINOACID_COLUMN] == aminoacid.name][get_data_from_file.PHI_COLUMN]), 
                    list(df[df[get_data_from_file.AMINOACID_COLUMN] == aminoacid.name][get_data_from_file.PSI_COLUMN]), 
                    color=aminoacid.color, label=aminoacid.name)
    
    ax.plot(plot_parameters.X_AXIS_LIM, [0, 0], color="black")
    ax.plot([0, 0], plot_parameters.Y_AXIS_LIM, color="black")    

    ax.set_xlim(plot_parameters.X_AXIS_LIM)
    ax.set_ylim(plot_parameters.Y_AXIS_LIM)
    ax.set_xlabel(plot_parameters.X_LABEL_TEXT)
    ax.set_ylabel(plot_parameters.Y_LABEL_TEXT)
    ax.grid()
    
    ax.set_title(title)
    ax.legend(bbox_to_anchor=(1.27, 1.1), loc='upper right', ncol=1)
    
    return fig

def save_ordinary_plot(df, aminoacids, title, save_path, file_name_ending):
    fig = get_ordinary_plot(df, aminoacids, title)
    full_file_name = title + "_" + file_name_ending + ".png"
    fig.savefig(save_path / full_file_name, bbox_inches='tight', pad_inches=0.5)
    
def draw_ordinary_plot(df, aminoacids, title):
    fig = get_ordinary_plot(df, aminoacids, title)
    plt.show()