from dataclasses import dataclass

# Aminoacid - a class for aminoacids with their three-letter code and color by which to draw them on the plot 
# AMINO_ACIDS - a tuple with the 20 most common amino acids in proteins

@dataclass()
class Aminoacid:
    name: str
    color: str

AMINO_ACIDS = (Aminoacid("ARG", "#E0FFFF"), Aminoacid("LYS", "#AFEEEE"), Aminoacid("ASP", "#7FFFD4"), 
               Aminoacid("GLU", "#40E0D0"), Aminoacid("GLN", "#00CED1"), Aminoacid("ASN", "#5F9EA0"), 
               Aminoacid("HIS", "#4682B4"), Aminoacid("SER", "#B0C4DE"), Aminoacid("THR", "#B0E0E6"), 
               Aminoacid("CYS", "#ADD8E6"), Aminoacid("TRP", "#87CEEB"), Aminoacid("MET", "#87CEFA"), 
               Aminoacid("ALA", "#00BFFF"), Aminoacid("ILE", "#1E90FF"), Aminoacid("LEU", "#6495ED"), 
               Aminoacid("PHE", "#7B68EE"), Aminoacid("VAL", "#4169E1"), Aminoacid("PRO", "#191970"), 
               Aminoacid("GLY", "#20B2AA"), Aminoacid("TYR", "#66CDAA"))