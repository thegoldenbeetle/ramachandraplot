import pandas as pd
import math
from Bio.PDB import PDBParser, PPBuilder

CHAIN_ID_COLUMN = "chain"
AMINOACID_COLUMN = "aminoacid"
PHI_COLUMN = "phi"
PSI_COLUMN = "psi"

# function for parsing pdb_file, getting from it phi and psi angles, converting it to degrees
# return pandas.DataFrame with colomns: chain (chain id), aminacid (three-letter code), phi (phi angle), psi (psi angle)

def create_dataframe(pdb_file):
    df = pd.DataFrame(columns=[CHAIN_ID_COLUMN, AMINOACID_COLUMN, PHI_COLUMN, PSI_COLUMN]);
    structure = PDBParser().get_structure(id=None, file=pdb_file)
    for model in structure:
        for chain in model:
            polypeptides = PPBuilder().build_peptides(chain)
            for peptide in polypeptides:
                phi_psi_list = peptide.get_phi_psi_list()
                for index, aminoacid in enumerate(peptide):
                    phi, psi = phi_psi_list[index]
                    if phi and psi:
                        df = df.append({CHAIN_ID_COLUMN: chain.id, AMINOACID_COLUMN: aminoacid.resname,
                                        PHI_COLUMN: math.degrees(phi), PSI_COLUMN: math.degrees(psi)},
                                         ignore_index=True)
    return df