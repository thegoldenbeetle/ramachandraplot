import plotly.graph_objects as go
import plotly.offline as pyo
from ipywidgets import widgets
from dataclasses import dataclass
from IPython.display import display

from aminoacid import AMINO_ACIDS
from plot_parameters import X_AXIS_LIM, Y_AXIS_LIM
from plot_parameters import X_LABEL_TEXT, Y_LABEL_TEXT
from plot_parameters import PLOT_HEIGHT, PLOT_WIDTH
from get_data_from_file import CHAIN_ID_COLUMN, AMINOACID_COLUMN, PHI_COLUMN, PSI_COLUMN

def create_figure_widget(df):
    layout = go.Layout(xaxis=dict(range=X_AXIS_LIM, nticks=9, title=dict(text=X_LABEL_TEXT)),
                       yaxis=dict(range=Y_AXIS_LIM, nticks=9, title=dict(text=Y_LABEL_TEXT)),
                       height=PLOT_HEIGHT, width=PLOT_WIDTH)
    scatters = [go.Scatter(x=list(df[df[AMINOACID_COLUMN] == aminoacid.name][PHI_COLUMN]), 
                           y=list(df[df[AMINOACID_COLUMN] == aminoacid.name][PSI_COLUMN]), 
                           mode='markers', marker_color=aminoacid.color, name=aminoacid.name)
                           for aminoacid in AMINO_ACIDS]
    return go.FigureWidget(data=scatters, layout=layout)

CHECK_ALL_ACIDS_STR = "Check all acids"
UNCHECK_ALL_ACIDS_STR = "Uncheck all acids"
CHECK_ALL_CHAINS_STR = "Check all chains"
UNCHECK_ALL_CHAINS_STR = "Uncheck all chains"

@dataclass()
class CheckUncheckAllButton:
    name: str
    button: widgets.Button
    checkboxes: list
    value: bool

def create_check_uncheck_buttons(aminoacidsCheckboxes, chainsCheckboxes):
    checkAllAcidsButton = widgets.Button(description=CHECK_ALL_ACIDS_STR, disabled=False)
    uncheckAllAcidsButton = widgets.Button(description=UNCHECK_ALL_ACIDS_STR, disabled=False)
    checkAllChainsButton = widgets.Button(description=CHECK_ALL_CHAINS_STR, disabled=False)
    uncheckAllChainsButton = widgets.Button(description=UNCHECK_ALL_CHAINS_STR, disabled=False)
    
    return {checkAllAcidsButton.description: CheckUncheckAllButton(checkAllAcidsButton.description, checkAllAcidsButton,
                                                                   aminoacidsCheckboxes, True),
            uncheckAllAcidsButton.description: CheckUncheckAllButton(uncheckAllAcidsButton.description, uncheckAllAcidsButton,
                                                                     aminoacidsCheckboxes, False),
            checkAllChainsButton.description: CheckUncheckAllButton(checkAllChainsButton.description, checkAllChainsButton,
                                                                    chainsCheckboxes, True),
            uncheckAllChainsButton.description: CheckUncheckAllButton(uncheckAllChainsButton.description, uncheckAllChainsButton,
                                                                      chainsCheckboxes, False)}

def draw_interactive_plot(dataframe, title):

    df = dataframe
    
    aminoacidsCheckboxes = [widgets.Checkbox(description=aminoacid.name, value=True, indent=False) 
                            for aminoacid in AMINO_ACIDS]
    chainsCheckboxes = [widgets.Checkbox(description=chain, value=True, indent=False) for chain in df[CHAIN_ID_COLUMN].unique()]
    
    checkUncheckButtons = create_check_uncheck_buttons(aminoacidsCheckboxes, chainsCheckboxes)
    
    f = create_figure_widget(df)
    
    aminoacidsHBox1 = widgets.HBox(children=aminoacidsCheckboxes[:10])
    aminoacidsHBox2 = widgets.HBox(children=aminoacidsCheckboxes[10:])
    
    chainsHBox = widgets.HBox(children=chainsCheckboxes)
    
    checkUncheckAllAcidsHBox = widgets.HBox(children=[checkUncheckButtons[CHECK_ALL_ACIDS_STR].button, 
                                                      checkUncheckButtons[UNCHECK_ALL_ACIDS_STR].button])
    checkUncheckAllChainsHBox = widgets.HBox(children=[checkUncheckButtons[CHECK_ALL_CHAINS_STR].button,
                                                       checkUncheckButtons[UNCHECK_ALL_CHAINS_STR].button])
            
    resultWidget = widgets.VBox([widgets.Label(value="Aminoacids: "), checkUncheckAllAcidsHBox, aminoacidsHBox1, aminoacidsHBox2,
                      widgets.Label(value="Chains: "), checkUncheckAllChainsHBox, chainsHBox,
                      widgets.Label(value="Ramachandran plot: " + title), f])
    
    def response(change):
        
        selectedChainsCheckboxes = list(filter(lambda checkBox: checkBox.value, chainsCheckboxes))
        selectedChains = list(map(lambda checkBox: checkBox.description, selectedChainsCheckboxes))        
        filtered_chain_df = df[df["chain"].isin(selectedChains)]
                
        with f.batch_update():           
            for acid_index in range(len(aminoacidsCheckboxes)):
                f.data[acid_index].x = list(
                    filtered_chain_df[df[AMINOACID_COLUMN] == aminoacidsCheckboxes[acid_index].description][PHI_COLUMN]
                )
                f.data[acid_index].y = list(
                    filtered_chain_df[df[AMINOACID_COLUMN] == aminoacidsCheckboxes[acid_index].description][PSI_COLUMN]
                )
                f.data[acid_index].visible = aminoacidsCheckboxes[acid_index].value
            
    def checkUncheckAction(button):
        for aminoacidCheckbox in checkUncheckButtons[button.description].checkboxes:
            aminoacidCheckbox.value = checkUncheckButtons[button.description].value
            
    for chainCheckbox in chainsCheckboxes:
        chainCheckbox.observe(response, names="value")
        
    for aminoacidCheckbox in aminoacidsCheckboxes:
        aminoacidCheckbox.observe(response, names="value")
    
    for buttonItem in checkUncheckButtons.values():
        buttonItem.button.on_click(checkUncheckAction)

    display(resultWidget)