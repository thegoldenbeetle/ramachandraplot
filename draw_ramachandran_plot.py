import argparse
from pathlib import Path
import textwrap
import sys

sys.path.insert(0, 'src/')
import get_data_from_file
from draw_ordinary_plot import draw_ordinary_plot
from draw_ordinary_plot import save_ordinary_plot
from aminoacid import AMINO_ACIDS
            
# df - DataFrame with colomns: chain (chain id), aminacid (three-letter code), phi (phi angle), psi (psi angle)
# acid_types - list with three-letters codes of amino acids which will be drown on the plot
# separate_chains - if True, draw multiple plots, one for each chain
# title - compound name
# save_path - path to dir where to save plots. If None - plot will be only shown.
# file_name_ending - file name for the result file will be: 
#                    pdb_file name + "_chain_id" (if with separate chain) + _file_name_ending + ".png"

def get_ordinary_ramachandran_plot(df, acid_types, separate_chains, title, save_path=None, file_name_ending=None):
    if separate_chains:
        chains = df[get_data_from_file.CHAIN_ID_COLUMN].unique()
        for chain in chains:
            filteredDf = df[df[get_data_from_file.CHAIN_ID_COLUMN].isin([chain])]
            if save_path:
                save_ordinary_plot(filteredDf, acid_types, title + "_chain" + str(chain), 
                                   save_path, file_name_ending)
            else:
                draw_ordinary_plot(filteredDf, acid_types, title + "_chain" + str(chain))
    else:
        if save_path:
            save_ordinary_plot(df, acid_types, title, save_path, file_name_ending)
        else:
            draw_ordinary_plot(df, acid_types, title)       

            
def init_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f','--pdb_file', required=True, help='Path to pdb file')
    parser.add_argument('-a','--acid_types', nargs='+', choices=list(map(lambda aminoacid: aminoacid.name, AMINO_ACIDS)),
                        default=list(map(lambda aminoacid: aminoacid.name, AMINO_ACIDS)), 
                        help='Amino acids for drawing')
    parser.add_argument('-sd','--save_dir', default="./plots", required=False, help='Path where to save plots')
    parser.add_argument('-fe','--file_name_ending', default="", required=False, 
                        help='''Ending of the filename for the saved plot 
                                (pdb_file name + "_chain" + chain_id (if with separate chain) + _file_name_ending + ".png")''')
    parser.add_argument('-sc','--separate_chains', default=False, action='store_true', help='Draw multiple plots, one for each chain')
    return parser


if __name__ == '__main__':
    
    parser = init_argument_parser()
    args = parser.parse_args()
    
    Path(args.save_dir).mkdir(parents=True, exist_ok=True)
    pdb_file = Path(args.pdb_file)
    
    df = get_data_from_file.create_dataframe(pdb_file)
    get_ordinary_ramachandran_plot(df, args.acid_types, args.separate_chains, pdb_file.stem, Path(args.save_dir), args.file_name_ending)
