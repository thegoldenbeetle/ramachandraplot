biopython
plotly==4.5.4
matplotlib
pandas
dataclasses
ipywidgets==7.5.1